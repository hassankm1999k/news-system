const request = require('supertest');
const { Category } = require('../../models/category');
const { User } = require('../../models/user');

let server;

describe('Categories', () => {
    beforeEach(() => {
        server = require('../../index');
    });

    afterEach(async () => {
        await Category.remove({});
        await server.close();
    });

    describe('GET /', () => {
        it('should return list of categories', async () => {
            await Category.collection.insertMany([
                { name: 'cat1', isDeleted: false },
                { name: 'cat2', isDeleted: false }
            ]);

            const res = await request(server).get('/api/categories');

            expect(res.status).toBe(200);
            expect(res.body.result.some(e => e.name == 'cat1')).toBeTruthy();
            expect(res.body.result.some(e => e.name == 'cat2')).toBeTruthy();
        });

        it('should return paginated list of categories', async () => {
            const pagination = {
                page: 1,
                limit: 10,
                total_count: 2,
                total_pages: 1
            };
            await Category.collection.insertMany([
                { name: 'cat1', isDeleted: false },
                { name: 'cat2', isDeleted: false }
            ]);

            const res = await request(server).get('/api/categories');

            expect(res.body).toHaveProperty('pagination');
            expect(res.body.pagination).toMatchObject(pagination);
        });
    });

    describe('GET /:id', () => {
        let category = new Category({
            name: 'cat1',
            isDeleted: false
        });

        let categoryId = category._id;

        let errorResponse = {
            success: false,
            errors: 'error message'
        };

        const exec = async () => {
            return await request(server).get(`/api/categories/${categoryId}`);
        };

        it('should return a category if valid id is passed', async () => {
            await category.save();

            const res = await exec();

            expect(res.status).toBe(200);
            expect(res.body.result).toEqual({
                _id: category.id,
                name: category.name
            });
        });

        it('should returns an error if invalid id is passed', async () => {
            categoryId = '1234';
            errorResponse.errors = 'Passed object ID is not valid.';

            const res = await exec();

            expect(res.status).toBe(400);
            expect(res.body).toMatchObject(errorResponse);
        });

        it('should returns a not found error if category does not exist', async () => {
            categoryId = '5e4a438001919625f5aa0c4d';
            errorResponse.errors = 'Category is not found!';

            const res = await exec();

            expect(res.status).toBe(404);
            expect(res.body).toMatchObject(errorResponse);
        });
    });

    describe('POST /', () => {
        let token;
        let categoryName;

        let exec = async () => {
            return await request(server)
                .post('/api/categories')
                .set('x-auth-token', token)
                .send({ name: categoryName });
        };

        beforeEach(() => {
            token = new User().generateAuthToken();
            categoryName = 'cat1';
        });

        it('should create the category if it is valid', async () => {
            const res = await exec();

            const category = await Category.find({ name: categoryName });

            expect(category).not.toBeNull();
        });

        it('should return the category in the response if it is valid', async () => {
            const res = await exec();

            expect(res.status).toBe(200);
            expect(res.body.success).toBe(true);
            expect(res.body.result).toHaveProperty('_id');
            expect(res.body.result).toHaveProperty('name', categoryName);
        });

        it('should return 401 if not authenticated', async () => {
            token = '';

            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return 400 if the category name is not provided', async () => {
            categoryName = '';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 400 if the category name is less than 3 chars', async () => {
            categoryName = 'hi';

            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return 400 if the category name is greater than 50 chars', async () => {
            categoryName = Array(52).join('a');

            const res = await exec();

            expect(res.status).toBe(400);
        });
    });
});
