const request = require('supertest');
const { User } = require('../../models/user');

let server;

describe('auth middleware', () => {
    let token;
    let user;

    let exec = () => {
        return request(server).get('/api/users/me').set('x-auth-token', token);
    };

    beforeEach(async () => {
        server = require('../../index');
        user = new User({
            name: 'test',
            email: 'test@test.com',
            password: 'secret'
        });
        await user.save();
        token = user.generateAuthToken();
    });

    afterEach(async () => {
        await User.remove({});
        await server.close();
    });

    it('returns 200 if token is valid', async () => {
        const res = await exec();

        expect(res.status).toBe(200);
    });

    it('returns 401 if token is not provided', async () => {
        token = '';

        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('returns 400 if token is not valid', async () => {
        token = 'a';

        const res = await exec();

        expect(res.status).toBe(400);
    });
});
