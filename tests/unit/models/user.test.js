const { User } = require('../../../models/user');
const jwt = require('jsonwebtoken');
const config = require('config');
const mongoose = require('mongoose');

describe('User', () => {
    it('should generate a valid token', () => {
        const payload = { _id: mongoose.Types.ObjectId(1).toHexString()};
        const user = new User(payload);
        const token = user.generateAuthToken();
        const decoded = jwt.verify(token, config.get('tokenSecretKey'));

        expect(decoded).toMatchObject(payload);
    });
});