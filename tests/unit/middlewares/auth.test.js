const auth = require('../../../middlewares/auth');
const { User } = require('../../../models/user');
const mongoose = require('mongoose');

describe('auth middleware', () => {
    
    it('populate user data in request body if token is valid', () => {
        const user = { _id: mongoose.Types.ObjectId().toHexString() };
        const token = new User(user).generateAuthToken();

        const req = {
            header: jest.fn().mockReturnValue(token)
        }
        const next = jest.fn();
        const res = {};

        auth(req, res, next);

        expect(req.user).toMatchObject(user);
    });
});