const { createLogger, transports, format } = require('winston');
const config = require('config');

const consoleTransport = new transports.Console({
    format: format.combine(
        format.colorize(),
        format.simple()
    )
});

const logger = createLogger({
    transports: [
        consoleTransport,
        new transports.File({ filename: config.get('logging.default') })
    ],
    exceptionHandlers: [
        consoleTransport,
        new transports.File({ filename: config.get('logging.exceptions') })
    ]
});

module.exports = logger;