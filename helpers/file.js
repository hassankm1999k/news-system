const config = require('config');
const fs = require('fs');

const photoRemover = (fileName) => {
    fs.unlink(config.get('storage_path') + '/' + fileName, (err) => {
        if (err) throw err;
    })
}

module.exports.photoRemover = photoRemover;