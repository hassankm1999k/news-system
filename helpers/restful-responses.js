const successResponse = (result, pagination = null) => {
    let response = {
        success: true,
        result
    };

    if (pagination) {
        response.pagination = {
            page: pagination.page,
            limit: pagination.limit,
            total_count: pagination.total,
            total_pages: pagination.pages,
        };
    }

    return response;
}

const errorResponse = (errors) => {
    return {
        success: false,
        errors
    };
}

module.exports.successResponse = successResponse;
module.exports.errorResponse = errorResponse;