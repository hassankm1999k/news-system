const express = require('express');
const logger = require('./helpers/logger');
const config = require('config');
const app = express();

// configure 
require('./startup/logging')();
require('./startup/config')();
require('./startup/routes')(app);
require('./startup/database')();

// startup server
const port = process.env.PORT || config.get('app_port');
const server = app.listen(port, () => {
    logger.info(`Listening on port: ${port}`);
});

module.exports = server;