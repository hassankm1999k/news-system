const mongoose = require('mongoose');
const { errorResponse } = require('../helpers/restful-responses');

module.exports = (req, res, next) => {

    if (! mongoose.Types.ObjectId.isValid(req.params.id))
      return res
        .status(400)
        .send(errorResponse('Passed object ID is not valid.'));
    
    next();
}