const { errorResponse } = require('../helpers/restful-responses');

module.exports = (req, res, next) => {
    res.status(404).send(
        errorResponse('The endpoint you are looking for could not be found!')
    );
}