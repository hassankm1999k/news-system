const multer = require('multer');
const config = require('config');
const uuidv4 = require('uuid/v4');
const InvalidFileFormat = require('../errors/InvalidFileFormat');

const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, config.get('storage_path'));
    },
    filename: (req, file, callback) => {
        callback(null, uuidv4() + '.' + getFileExtention(file));
    }
});
const upload = multer({
    storage: storage,
    fileFilter: (req, file, callback) => {
        if (getFileExtention(file) !== 'jpg' && getFileExtention(file) !== 'jpeg') {

            return callback(new InvalidFileFormat(
                    getFileExtention(file) + ' photo format is not supported. Only jpg format is accepted.'
                ), false)
        }

        callback(null, true)
    }
});

function getFileExtention(file) {

    let splitedMimeType = file.mimetype.split('/');
    const ext = splitedMimeType[splitedMimeType.length - 1];
    return ext;

}

module.exports = upload;