const jwt = require('jsonwebtoken');
const config = require('config');
const { errorResponse } = require('../helpers/restful-responses');

module.exports = (req, res, next) => {
    const token = req.header('x-auth-token');

    if (! token) {
        return res
          .status(401)
          .send(errorResponse('Access Denied. Token not provided!'));
    }

    try {
        const payload = jwt.verify(token, config.get('tokenSecretKey'));
        req.user = payload;
        next();
    } catch (ex) {
        res.status(400).send(errorResponse('Invalid token.'));
    }
}