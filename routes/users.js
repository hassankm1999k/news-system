const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();

const { User, validate, validateAuth } = require('../models/user');
const { successResponse, errorResponse } = require('../helpers/restful-responses');
const auth = require('../middlewares/auth');
const _ = require('lodash');

// Endpoints
router.post('/', async (req, res) => {
    const { error } = validate(req.body);
    if (error) {
        const errors = _.map(
            error.details, 
            e => e.message
        );
        return res.status(400).send(
            errorResponse(errors)
        );
    }

    const user = new User(_.pick(req.body, [
        'name',
        'email',
        'password'
    ]));

    // hash password
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);    

    await user.save();

    // generate token
    const token = user.generateAuthToken();

    res.header('x-auth-token', token).send(
        successResponse(
            user.getExposedAttributes()
        )
    );
});

router.post('/auth', async (req, res) => {
    const { error } = validateAuth(req.body);
    if (error) {
        const errors = _.map(
            error.details,
            e => e.message
        );
        return res.status(400).send(
            errorResponse(errors)
        );
    }

    const user = await User.findOne({ email: req.body.email });
    if (! user) return res.status(400).send(errorResponse("Invalid email or password."))
    
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (! validPassword) return res.status(400).send(errorResponse('Invalid email or password.'));

    const token = user.generateAuthToken();
    res.header('x-auth-token', token).send(successResponse({ token }));
});

router.get('/me', auth, async (req, res) => {
    const user = await User.findById(req.user._id);
    res.send(successResponse(user.getExposedAttributes()));
});

module.exports = router;