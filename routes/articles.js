const express = require('express');
const router = express.Router();
const _ = require('lodash');

const { Article, validate } = require('../models/article');
const { Category } = require('../models/category');
const { successResponse, errorResponse } = require('../helpers/restful-responses');
const upload = require('../middlewares/fileupload');
const { photoRemover } = require('../helpers/file');
const validateObjectId = require('../middlewares/validateObjectId');
const auth = require('../middlewares/auth');


router.get('/', async (req, res) => {

    const limit = parseInt(req.query.limit) || 10;
    const page = parseInt(req.query.page) || 1;
    const total = await Article.find({ isDeleted: false }).count();
    const pages = Math.ceil(total / limit);

    articles = await Article.find({ isDeleted: false })
        .skip((page - 1) * limit)
        .limit(limit)
        .sort('title');

    const data = _.map(articles, article => article.getExposedAttributes());

    res.send(
        successResponse(data, { total, pages, page, limit })
    );

});

router.get('/:id', validateObjectId, async (req, res) => {

    const article = await Article.findOne({ _id: req.params.id, isDeleted: false });
    if (!article) {
        return res.status(404).send(
            errorResponse('Article is not found!')
        );
    }

    res.send(
        successResponse(article.getExposedAttributes())
    );

});

router.post('/', auth, upload.single('photo'), async (req, res) => {
    const { error } = validate(req.body);
    if (error) {
        const errors = _.map(
            error.details,
            error => error.message
        );
        if (req.file) photoRemover(req.file.filename);
        return res.status(400).send(
            errorResponse(errors)
        );
    }


    const category = await Category.findById(req.body.category_id);
    if (!category) {
        if (req.file)
            photoRemover(req.file.filename)
        return res.status(400).send(
            errorResponse("Invalid Category id."));
    }

    const article = new Article({
        title: req.body.title,
        content: req.body.content,
        category_id: req.body.category_id
    });

    if (req.file) {
        article.photo = req.file.filename;
    }
    await article.save();

    res.send(
        successResponse(article.getExposedAttributes())
    );
});


router.put('/:id', auth, validateObjectId, upload.single('photo'), async (req, res) => {

    const { error } = validate(req.body);
    if (error) {
        const errors = _.map(
            error.details,
            error => error.message
        );
        if (req.file)
            photoRemover(req.file.filename)
        return res.status(400).send(errorResponse(errors));
    }

    const category = await Category.findById(req.body.category_id);
    if (!category) {
        if (req.file)
            photoRemover(req.file.filename)
        return res.status(400).send(
            errorResponse('Invalid Category id.')
        );
    }


    if (req.file) {
        let article = await Article.findById(req.params.id);
        if (article) photoRemover(article.photo);
    }

    const updatedArticle = _.pick(req.body, [
        'title',
        'content',
        'photo',
        'category_id'
    ]);

    if (req.file)
        updatedArticle.photo = req.file.filename;

    const article = await Article.findByIdAndUpdate(
        { _id: req.params.id, isDeleted: false },
        {
            $set: updatedArticle
        },
        { new: true }
    );


    if (!article) {
        if (req.file)
            photoRemover(req.file.filename)
        return res.status(404).send(
            errorResponse('Article is not found!')
        );
    }

    res.send(
        successResponse(article.getExposedAttributes())
    );

});

router.delete('/:id', auth, validateObjectId, async (req, res) => {

    const article = await Article.findByIdAndUpdate(
        { _id: req.params.id, isDeleted: false },
        { isDeleted: true },
        { new: true }
    );

    if (!article) return res.status(404).send(
        errorResponse('Article not found')
    );
    
    if (article.photo) photoRemover(article.photo);
    
    res.send(
        successResponse(article.getExposedAttributes())
    );
});

module.exports = router;
