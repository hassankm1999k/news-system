const express = require('express');
const router = express.Router();
const _ = require('lodash');

const { Category, validate } = require('../models/category');
const { Article } = require('../models/article');
const { successResponse, errorResponse } = require('../helpers/restful-responses');
const validateObjectId = require('../middlewares/validateObjectId');
const auth = require('../middlewares/auth');

// Endpoints
router.get('/', async (req, res) => {

    const limit = parseInt(req.query.limit) || 10;
    const page = parseInt(req.query.page) || 1;
    const total = await Category.find({ isDeleted: false }).count();
    const pages = Math.ceil(total / limit);

    const categories = await Category.find({ isDeleted: false })
        .skip((page - 1) * limit)
        .limit(limit)
        .sort('name');

    const data = _.map(
        categories,
        category => category.getExposedAttributes()
    );

    res.send(
        successResponse(data, { total, pages, page, limit })
    );
});

router.get('/:id', validateObjectId, async (req, res) => {

    const category = await Category.findOne({ _id: req.params.id, isDeleted: false })
    if (!category) return res.status(404).send(
        errorResponse('Category is not found!')
    );

    res.send(
        successResponse(category.getExposedAttributes())
    );
});

router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) {
        const errors = _.map(
            error.details,
            error => error.message
        );
        return res.status(400).send(
            errorResponse(errors)
        );
    }

    const category = new Category({
        name: req.body.name
    });

    await category.save();

    res.send(
        successResponse(category.getExposedAttributes())
    );
});

router.put('/:id', auth, validateObjectId, async (req, res) => {
    const { error } = validate(req.body);
    if (error) {
        const errors = _.map(error.details, error => error.message);
        return res.status(400).send(errorResponse(errors));
    }

    const category = await Category.findByIdAndUpdate(
        { _id: req.params.id, isDeleted: false },
        { $set: _.pick(req.body, ['name']) },
        { new: true }
    );

    if (!category) return res.status(404).send(errorResponse('Category not found!'));

    res.send(successResponse(category.getExposedAttributes()));
});

router.delete('/:id', auth, validateObjectId, async (req, res) => {

    const articleMatched = await Article.count({ category_id: req.params.id, isDeleted: false });

    if (articleMatched !== 0)
        return res.status(400).send(
            errorResponse("Can't delete this category, it contains articles.")
        );

    const category = await Category.findByIdAndUpdate(
        { _id: req.params.id, isDeleted: false },
        { isDeleted: true },
        { new: true }
    );

    if (!category) return res.status(404).send(errorResponse('Category not found!'));

    res.send(successResponse(category.getExposedAttributes()));
});

module.exports = router;