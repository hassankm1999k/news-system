const config = require('config');
const Joi = require('@hapi/joi');
const JoiObjectId = require('joi-objectid')(Joi);

module.exports = () => {

  // check if token secret key exist
  if (!config.get('tokenSecretKey')) {
    throw new Error('Token Secret key is not set!');
  }

  // extend Joi validator
  Joi.objectId = JoiObjectId;

  // run hard delete Job
  if (config.get('runHardDeleteJob') == true) {
    require('./hardDeleteJob')();
  }

};
