require('express-async-errors');
const express = require('express');

const articles = require('../routes/articles');
const categories = require('../routes/categories');
const users = require('../routes/users');
const notFoundHandler = require('../middlewares/notFound');
const errorHandler = require('../middlewares/error');

module.exports = function (app) {

    app.use(express.json());
    
    // Main endpoints
    app.use('/api/categories', categories);
    app.use('/api/articles', articles);
    app.use('/api/users', users);

    // 404 handler
    app.use(notFoundHandler);
    
    // custom error handler
    app.use(errorHandler);

}