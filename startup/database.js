const mongoose = require('mongoose');
const config = require('config');
const logger = require('../helpers/logger');

module.exports = function() {

  // prepare connection string
  const connectionString = `${config.get('database.type')}://${config.get(
    'database.host'
  )}:${config.get('database.port')}/${config.get('database.name')}`;

  // connect to database
  mongoose
    .connect(connectionString, {
      useUnifiedTopology: true,
      useNewUrlParser: true
    })
    .then(() => {
      logger.info(`Connected to ${config.get('database.name')} successfuly ..`);
    });
};
