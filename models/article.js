const mognoose = require('mongoose');
const Joi = require('@hapi/joi');
const _ = require('lodash');


const articleSchema = new mognoose.Schema({
    title: {
        type: String,
        required: true,
        minLength: 3,
        maxLength: 100,
        trim: true
    },
    content: {
        type: String,
        required: true,
        minLength: 5,
        maxLength: 5000
    },
    photo: String,
    category_id: {
        type: mognoose.Schema.Types.ObjectId,
        ref: 'Category'
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
});

function validateArticle(article) {

    const schema = Joi.object({
        title: Joi.string()
            .required()
            .min(3)
            .max(100)
            .trim(),
        content: Joi.string()
            .required()
            .min(3)
            .max(5000),
        category_id: Joi.objectId()
            .required()
    });

    return schema.validate(article, {
        abortEarly: false
    });
}

// define exposed attributes
articleSchema.statics.exposed = [
        '_id',
        'title',
        'content',
        'photo',
        'category_id'
    ];
articleSchema.methods.getExposedAttributes = function () {
    return _.pick(this, this.constructor.exposed);
};

const Article = mognoose.model('Article', articleSchema);

module.exports.Article = Article;
module.exports.validate = validateArticle;