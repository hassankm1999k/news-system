const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
const _ = require('lodash');

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50,
        trim: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
});

// define exposed attributes
categorySchema.statics.exposed = [
        '_id',
        'name'
    ];

categorySchema.methods.getExposedAttributes = function () {
    return _.pick(this, this.constructor.exposed);
};

const Category = mongoose.model('Category', categorySchema);

const joiSchema = Joi.object({
    name: Joi.string().required().min(3).max(50)
});

const validate = (category) => {
    return joiSchema.validate(category, {
        abortEarly: false
    });
}

module.exports.Category = Category;
module.exports.validate = validate;