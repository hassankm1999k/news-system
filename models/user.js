const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
const config = require('config');
const jwt = require('jsonwebtoken')
const _ = require('lodash');

const userScheme = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 3,
        max: 50
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true
    }
});

// define exposed attributes
userScheme.statics.exposed = [
    '_id',
    'name',
    'email'
];

userScheme.methods.getExposedAttributes = function () {
    return _.pick(this, this.constructor.exposed);
}

userScheme.methods.generateAuthToken = function() {
    return jwt.sign({ _id: this._id }, config.get('tokenSecretKey'));
}

const User = mongoose.model('User', userScheme);

//validate user object
const joiSchema = Joi.object({
    name: Joi.string().required().min(3).max(50),
    email: Joi.string().email().required(),
    password: Joi.string().required()
})

const validate = (user) => {
    return joiSchema.validate(user, {
        abortEarly: false
    });
}

// validate auth requests
const joiAuthSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required()
});

const validateAuth = authInfo => {
  return joiAuthSchema.validate(authInfo, {
    abortEarly: false
  });
};


module.exports = {
    User,
    validate,
    validateAuth
}